package main

import (
	"fmt"
	"net/http"
	"sync"
	"time"
)

func main() {
	fetchImages(true)
}

// Посмотреть все изображения по ссылкам
func fetchImages(async bool) {
	urls := []string{
		"https://www.letu.ru/common/img/uploaded/productImageFolder/MPL017482_mainLG.jpg",
		"https://www.letu.ru/common/img/uploaded/skuImageFolder/MPL017482_mainEX.jpg",
		"https://www.letu.ru/common/img/uploaded/skuImageFolder/MPL017484_mainEX.jpg",
		"https://www.letu.ru/common/img/uploaded/productImageFolder/MPL017483_mainLG.jpg",
		"https://www.letu.ru/common/img/uploaded/skuImageFolder/MPL017483_mainEX.jpg",
		"https://www.letu.ru/common/img/uploaded/productImageFolder/MPL017485_mainLG.jpg",
		"https://www.letu.ru/common/img/uploaded/skuImageFolder/MPL017485_mainEX.jpg",
		"https://www.letu.ru/common/img/uploaded/productImageFolder/MPL009985LG.jpg",
		"https://www.letu.ru/common/img/uploaded/skuImageFolder/MPL009985EX.jpg",
		"https://www.letu.ru/common/img/uploaded/skuImageFolder/MPL009984EX.jpg",

		"https://www.letu.ru/common/img/uploaded/productImageFolder/MPL009986LG.jpg",
		"https://www.letu.ru/common/img/uploaded/productImageFolder/MPL009986_2.jpg",
		"https://www.letu.ru/common/img/uploaded/skuImageFolder/MPL009986EX.jpg",
		"https://www.letu.ru/common/img/uploaded/productImageFolder/MPL009987LG.jpg",
		"https://www.letu.ru/common/img/uploaded/productImageFolder/MPL009988LG.jpg",
		"https://www.letu.ru/common/img/uploaded/productImageFolder/MPL009988_2.jpg",
		"https://www.letu.ru/common/img/uploaded/skuImageFolder/MPL009988EX.jpg",
		"https://www.letu.ru/common/img/uploaded/productImageFolder/MPL020499LG.jpg",
		"https://www.letu.ru/common/img/uploaded/skuImageFolder/MPL020499EX.jpg",
		"https://www.letu.ru/common/img/uploaded/productImageFolder/MPL020500LG.jpg",
	}

	beginTime := time.Now().UnixNano() / int64(time.Millisecond)

	var promises sync.WaitGroup

	for i, url := range urls {
		if async {
			promises.Add(1)

			go fetchImageAsync(i, url, &promises)
		} else {
			fetchImageNormally(i, url)
		}
	}

	if async {
		promises.Wait()
	}

	endTime := time.Now().UnixNano() / int64(time.Millisecond)

	fmt.Printf("DONE at %d ms \n", endTime-beginTime)
}

// Просмотр изображения с промисом
func fetchImageAsync(index int, url string, promises *sync.WaitGroup) {
	defer promises.Done()

	response, _ := http.Get(url)
	length := response.ContentLength

	fmt.Printf("Image #%d size is %d \n", index, length)
}

// Просмотр изображения в порядке очереди
func fetchImageNormally(index int, url string) {
	response, _ := http.Get(url)
	length := response.ContentLength

	fmt.Printf("Image #%d size is %d \n", index, length)
}
